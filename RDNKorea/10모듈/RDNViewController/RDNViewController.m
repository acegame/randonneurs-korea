//
//  RDNViewController.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "RDNViewController.h"

@interface RDNViewController ()
{
    UIView *deemView_;
}
@end

@implementation RDNViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    BKLog(@"%@", NSStringFromClass([self class]));
}

#pragma mark - setter
- (void)setAlpha:(CGFloat)alpha {
    static CGFloat const kMaxAlpha = 0.6;
    if(deemView_ == nil) {
        deemView_ = [[UIView alloc] initWithFrame:self.view.bounds];
        [deemView_ setBackgroundColor:[UIColor blackColor]];
        [deemView_ setAlpha:0];
    }
    
    if(alpha > 0) {
        [self.view addSubview:deemView_];
    }
    else {
        [deemView_ removeFromSuperview];
    }
    
    [deemView_ setAlpha:MIN(kMaxAlpha, alpha)];
}
@end

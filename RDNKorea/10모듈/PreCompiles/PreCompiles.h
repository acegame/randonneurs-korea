//
//  PreCompiles.h
//
//  Created by ByungKi Han on 14. 1. 28..
//  Copyright (c) 2014년 WAG. All rights reserved.
//

/*
 *  플랫폼 관련
 */
#define kScreenSize     [UIScreen mainScreen].bounds.size
#define kScreenWidth    [UIScreen mainScreen].bounds.size.width
#define kScreenHeight   [UIScreen mainScreen].bounds.size.height

#define kVersionShort   [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
#define kVersion        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
#define kVersionOS      [[UIDevice currentDevice] systemVersion]

#define kDocumentPath   [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

/*
 *  기타 매크로
 */
#define NSStringEqual(x,y)      [x isEqualToString:y]
#define NSStringNotEqual(x,y)   (!NSStringEqual((x),(y)))
#define NSStringEmpty(x)        ((x == nil) || (x.length == 0))
#define NSStringNotEmpty(x)     (!NSStringEmpty(x))
#define RadianToDegree(x)       (x * 180 / M_PI)
#define DegreeToRadian(x)       (x * M_PI / 180)

/*
 *  RGB
 */
#define RGB(R, G, B)        [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1]
#define RGBA(R, G, B, A)    [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:A]
#define RGBHEX(rgbValue)    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/*
 *  디버깅 메시지 관련
 */
#if _LOG_DB==1
#define DBLog( s, ... ) NSLog(@"[DB]%@", [NSString stringWithFormat:(s), ##__VA_ARGS__])
#else
#define DBLog( s, ... )
#endif

#if _LOG_NET==1
#define NETLog( s, ... ) NSLog(@"[NET]%@", [NSString stringWithFormat:(s), ##__VA_ARGS__])
#else
#define NETLog( s, ... )
#endif

#if _LOG_DEBUG==1
#define BKLog( s, ... ) NSLog(@"[BK]%s(%d) %@", __FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__])
#else
#define BKLog( s, ... )
#endif

#if _LOG_TEMP==1
#define BKTempLog( s, ... ) NSLog(s, ##__VA_ARGS__)
#else
#define BKTempLog( s, ... )
#endif


//
//  NSArray+BK.h
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (BK)

- (id)objAtIndex:(NSUInteger)index;

@end

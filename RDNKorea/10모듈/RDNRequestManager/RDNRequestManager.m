//
//  RDNRequestManager.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 16..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "RDNRequestManager.h"
#import "AFNetworking.h"

@implementation RDNRequestManager
+ (void)requestSigninWithId:(NSString *)clienId password:(NSString *)password done:(RDNRequestDoneBlock)doneBlock {
    NSDictionary *param = @{@"mb_id":clienId,
                            @"mb_password":password};
    AFHTTPRequestOperationManager *manager  = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.clien.net"]];
    [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithArray:@[@"text/html"]]];
    AFHTTPRequestOperation * operation      = [manager POST:@"/cs2/bbs/login_check.php"
                                                 parameters:param
                                                    success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                        doneBlock([[RDNUser shared] isSigned], nil, nil);
                                                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                        doneBlock([[RDNUser shared] isSigned], nil, nil);
                                                    }];
    [operation start];
}

@end

//
//  RDNTableViewController.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 17..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDNTableViewController : UITableViewController
@property (nonatomic, assign) CGFloat alpha;
@end

//
//  MenuViewController.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 10..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "MenuViewController.h"
#import "RDNMenuItem.h"
#import "MenuCell.h"
#import "AppDelegate.h"

#import "SelectEventVC.h"
#import "CyclienVC.h"

@interface MenuViewController () <UICollectionViewDataSource>
{
    NSArray *menuItems_;
}
@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems_ = @[[RDNMenuItem menuItemWithTitle:@"메인" storyboardIdentifierOfClass:@"MainViewController"],
                   [RDNMenuItem menuItemWithTitle:@"랜도너스" storyboardIdentifierOfClass:@"SelectEventVC"],
                   [RDNMenuItem menuItemWithTitle:@"클량자당" storyboardIdentifierOfClass:@"CyclienVC"],
                   [RDNMenuItem menuItemWithTitle:@"MY" storyboardIdentifierOfClass:@"SettingVC"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return menuItems_.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
    
    RDNMenuItem *item = [menuItems_ objectAtIndex:indexPath.item];
    [cell setItem:item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    RDNMenuItem *item = [menuItems_ objectAtIndex:indexPath.item];
    
    AppDelegate *delegate = ((AppDelegate *)[[UIApplication sharedApplication] delegate]);
    UINavigationController *navigationController = (UINavigationController *)delegate.drawerController.centerViewController;
    
    id viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:item.storyboardIdentifier];
    [navigationController setViewControllers:@[viewController] animated:YES];
    
    [delegate.drawerController closeDrawerAnimated:YES completion:nil];
}
@end

//
//  UIWebView+BK.m
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import "UIWebView+BK.h"

@implementation UIWebView (BK)

/*
 *  배경 투명 웹뷰 : 웹뷰처럼 보이지 않도록
 */
+ (UIWebView *)webViewOfBGClearWithFrame:(CGRect)frame
{
    UIWebView *webView_ = [[UIWebView alloc] initWithFrame:frame];
    
    [webView_ setBackgroundColor:[UIColor clearColor]];
    id scroller = [webView_.subviews objectAtIndex:0];
    for (UIView *subView in [scroller subviews])
    {
        if ([[[subView class] description] isEqualToString:@"UIImageView"])
        {
            subView.hidden = YES;
        }
        subView.backgroundColor = [UIColor clearColor];
        subView.opaque = NO;
    }
    
    return webView_;
}

@end

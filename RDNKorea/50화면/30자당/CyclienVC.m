//
//  Cyclien.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "CyclienVC.h"

static NSString *const kCookieId   = @"26deabb3e3c3deaa25a88a66edbd6860";
static NSString *const kCookiePass = @"65dc64aa36cc929e2be6287a4e2c06c7";
static NSString *const kMainURL    = @"http://m.clien.net/cs3/board?bo_table=cm_bike";

@interface CyclienVC () <UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *webView_;
}
@end

@implementation CyclienVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self onCyclienMain];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onCyclienMain {
    [webView_ loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kMainURL]]];
}

#pragma mark - UIWebView
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    BKLog(@"REQ : %@", request.URL.absoluteString);
    return [request.URL.absoluteString containsString:@"clien.net"];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    BKLog(@"------->");
    [RDNIndicator increase];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [RDNIndicator decrease];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [RDNIndicator decrease];
    [self testShowCookie];
    BKLog(@"<----------");
}

#pragma mark - private
- (void)testShowCookie {
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for(NSHTTPCookie *cookie in [cookieStorage cookies]) {
        NSLog(@"%@ : %@", cookie.name, cookie.value);
    }
    
    
}
@end

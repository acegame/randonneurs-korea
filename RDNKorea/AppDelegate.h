//
//  AppDelegate.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMDrawerController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) MMDrawerController *drawerController;

@end


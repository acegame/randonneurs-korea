//
//  EventCell.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "EventCell.h"
#import "RDNEvent.h"

@interface EventCell()
{
    __weak IBOutlet UIImageView *thumbnailImageView_;
    __weak IBOutlet UILabel *dateLabel_;
    __weak IBOutlet UILabel *nameLabel_;
}
@end

@implementation EventCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - setter
- (void)setEvent:(RDNEvent *)event {
    _event = event;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy년 MM월 dd일"];
    
    [thumbnailImageView_ setImageWithURL:[NSURL URLWithString:event.thumbnail]];
    [dateLabel_ setText:[formatter stringFromDate:event.date]];
    [nameLabel_ setText:event.name];
}

@end

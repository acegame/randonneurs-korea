//
//  MenuCell.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 10..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RDNMenuItem;

@interface MenuCell : UICollectionViewCell
@property (nonatomic, strong) RDNMenuItem *item;
@end

//
//  UIButton+BK.h
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (BK)

- (void)setImageForNormal:(UIImage *)normalImage forSelected:(UIImage *)selectedImage;
- (void)setImageForNormal:(UIImage *)normalImage forHighlighted:(UIImage *)highlightedImage;
- (void)setBGImageForNormal:(UIImage *)normalImage forSelected:(UIImage *)selectedImage;
- (void)setBGImageForNormal:(UIImage *)normalImage forHighlighted:(UIImage *)highlightedImage;
- (void)setTitleColorForNormal:(UIColor *)normalColor forSelected:(UIColor *)selectedColor;
- (void)setTitleForNormal:(NSString *)normalTitle forSelected:(NSString *)selectedTitle;

/*
 *  버튼 라벨과 이미지가 좌우로 정렬되어 있는 경우 버튼 사이즈 조정
 */
- (void)adjustsWidth;

/*
 *  이미지(위), 텍스트(아래) 형태의 센터 정렬 함수(spaceH는 이미지와 텍스트의 세로 간격, correctX는 이미지의 X좌표 보정값이다)
 */
- (void)adjustsVerticalCenterWithSpaceH:(CGFloat)spaceH correctX:(CGFloat)correctX;
- (void)adjustsVerticalCenterWithSpaceH:(CGFloat)spaceH;

/*
 *  버튼 라벨과 이미지의 우측정렬
 */
- (void)adjustsRight;

@end

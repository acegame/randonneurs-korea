//
//  NSString+BK.h
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (BK)

- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string options:(NSStringCompareOptions)options;
- (NSString *)urlEndoded;
- (NSString *)urlDecoded;

@end

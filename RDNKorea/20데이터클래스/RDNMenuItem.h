//
//  RDNMenuItem.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 10..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDNMenuItem : NSObject
@property (nonatomic, copy) NSString *storyboardIdentifier;
@property (nonatomic, copy) NSString *title;

+ (RDNMenuItem *)menuItemWithTitle:(NSString *)title storyboardIdentifierOfClass:(NSString *)storyboardIdentifier;
@end

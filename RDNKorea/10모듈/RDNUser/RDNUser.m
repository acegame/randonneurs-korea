//
//  RDNUser.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 16..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "RDNUser.h"

static NSString *const kService = @"kr.wag.RDNKorea.KEYCHAIN";

@implementation RDNUser
+ (RDNUser *)shared {
    static RDNUser *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[RDNUser alloc] init];
    });
    return shared;
}

- (void)clear {
    [RDNUser shared].clienId        = @"";
    [RDNUser shared].clienPassword  = @"";
    [RDNUser shared].cookieId       = @"";
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for(NSHTTPCookie *cookie in [cookieStorage cookies]) {
        [cookieStorage deleteCookie:cookie];
    }
}

#pragma mark - setter/getter
- (void)setClienId:(NSString *)clienId {
    [[NSUserDefaults standardUserDefaults] setValue:clienId forKey:@"clienId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString *)clienId {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"clienId"];
}

- (void)setClienPassword:(NSString *)clienPassword {
    [SSKeychain setPassword:clienPassword
                 forService:kService
                    account:[RDNUser shared].clienId];
}

- (NSString *)clienPassword {
    return [SSKeychain passwordForService:kService account:[RDNUser shared].clienId];
}

- (void)setShareMyLocation:(BOOL)shareMyLocation {
    [[NSUserDefaults standardUserDefaults] setValue:@(shareMyLocation) forKey:@"shareMyLocation"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)shareMyLocation {
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"shareMyLocation"] boolValue];
}

- (BOOL)isSigned {
    static NSString *const kCookieId   = @"26deabb3e3c3deaa25a88a66edbd6860";
    static NSString *const kCookiePass = @"65dc64aa36cc929e2be6287a4e2c06c7";
    
    BOOL cookieIdValid      = NO;
    BOOL cookiePassValid    = NO;
    
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for(NSHTTPCookie *cookie in [cookieStorage cookies]) {
        if([cookie.name isEqualToString:kCookieId]) {
            cookieIdValid = (cookie.value.length > 0);
            if(cookieIdValid) {
                [RDNUser shared].cookieId = cookie.value;
            }
        }
        
        if([cookie.name isEqualToString:kCookiePass]) {
            cookiePassValid = (cookie.value.length > 0);
        }
    }
    
    return (cookieIdValid && cookiePassValid);
}

- (BOOL)isIdPsswordExist {
    return ([[RDNUser shared] clienId].length > 0 && [[RDNUser shared] clienPassword].length > 0);
}
@end

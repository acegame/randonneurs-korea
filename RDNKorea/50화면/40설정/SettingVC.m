//
//  SettingVC.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 16..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "SettingVC.h"



@interface SettingVC () <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UIImageView *profileImageView_;
    __weak IBOutlet UILabel *clienIdLabel_;
    __weak IBOutlet UILabel *onTheSaddleIdLabel_;
    __weak IBOutlet UISwitch *shareMyLocationSwitch_;
    __weak IBOutlet UILabel *versionLabel_;
    
}
@end

@implementation SettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([RDNUser shared].isSigned) {
        [clienIdLabel_ setText:[RDNUser shared].clienId];
        [self requestUserInfo];
    }
    else {
        [clienIdLabel_ setText:@"로그인"];
        [onTheSaddleIdLabel_ setText:@""];
    }
    
    [profileImageView_ setImageWithURL:[NSURL URLWithString:@"http://si.wsj.net/public/resources/images/BN-BY925_mag041_OZ_20140318165119.jpg"]];
    [shareMyLocationSwitch_ setOn:[RDNUser shared].shareMyLocation];
    [versionLabel_ setText:[NSString stringWithFormat:@"ver %@", kVersionShort]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - UITableView
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 로그인 or 로그아웃
    if(indexPath.section == 0) {
        if([RDNUser shared].isSigned) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"On The Saddle"
                                                                           message:@"로그아웃 하시겠습니까?"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"취소"
                                                             style:UIAlertActionStyleDestructive
                                                           handler:^(UIAlertAction *action) {
                                                               
                                                           }];
            UIAlertAction *ok     = [UIAlertAction actionWithTitle:@"예"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               [[RDNUser shared] clear];
                                                               [clienIdLabel_ setText:@"로그인"];
                                                               [onTheSaddleIdLabel_ setText:@"로그인"];
                                                               [shareMyLocationSwitch_ setOn:NO animated:YES];
                                                           }];
            [alert addAction:cancel];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"On The Saddle"
                                                                           message:@"클리앙에 로그인해 주세요."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.placeholder = @"clien id";
            }];
            [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
                textField.secureTextEntry = YES;
                textField.placeholder = @"password";
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"취소"
                                                             style:UIAlertActionStyleDestructive
                                                           handler:^(UIAlertAction *action) {
                                                               
                                                           }];
            UIAlertAction *ok     = [UIAlertAction actionWithTitle:@"로그인"
                                                             style:UIAlertActionStyleDefault
                                                           handler:^(UIAlertAction *action) {
                                                               NSString *clienId   = [(UITextField *)alert.textFields[0] text];
                                                               NSString *clienPass = [(UITextField *)alert.textFields[1] text];
                                                               if(clienId.length > 0 && clienPass.length > 0) {
                                                                   [RDNRequestManager requestSigninWithId:clienId
                                                                                                 password:clienPass
                                                                                                     done:^(BOOL success, id data, id extra) {
                                                                                                         if(success) {
                                                                                                             [RDNUser shared].clienId = clienId;
                                                                                                             [RDNUser shared].clienPassword = clienPass;
                                                                                                             [clienIdLabel_ setText:clienId];
                                                                                                             [self requestUserInfo];
                                                                                                         }
                                                                                                         else {
                                                                                                             BKLog(@"로그인 실패");
                                                                                                         }
                                                                                                     }];
                                                               }
                                                           }];
            [alert addAction:cancel];
            [alert addAction:ok];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
}

#pragma mark - Event
- (IBAction)onSwitch:(UISwitch *)sender {
    [RDNUser shared].shareMyLocation = sender.isOn;
}

#pragma mark - Request
- (void)requestUserInfo {
    [RDNIndicator increase];
    PFQuery *query = [PFQuery queryWithClassName:@"USER"];
    [query whereKey:@"COOKIE_ID" equalTo:[RDNUser shared].cookieId];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if(error) {
            if(error.code == 101) {                 // On The Saddle 미등록 사용자
                [self requestUserRegistration];     // 사용자 등록
            }
            else {
                BKLog(@"%@", [error userInfo]);
            }
        }
        else {                                      // 이미 등록된 사용자
            [onTheSaddleIdLabel_ setText:object[@"NAME"]];
        }
        
        [RDNIndicator decrease];
    }];
}

- (void)requestUserRegistration {
    PFObject *object = [PFObject objectWithClassName:@"USER"];
    object[@"COOKIE_ID"] = [RDNUser shared].cookieId;
    object[@"NAME"] = @"닉네임";
    [RDNIndicator increase];
    [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if(succeeded) {
            [self requestUserInfo];
        }
        else {
            BKLog(@"사용자 등록 실패 : %@", [error userInfo]);
        }
        [RDNIndicator decrease];
    }];
}
@end

//
//  UIView+BK.h
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 WAG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BK)

@property (nonatomic, assign) CGFloat frameX;
@property (nonatomic, assign) CGFloat frameY;
@property (nonatomic, assign) CGFloat frameHeight;
@property (nonatomic, assign) CGFloat frameWidth;
@property (nonatomic, assign) CGPoint frameOrigin;
@property (nonatomic, assign) CGSize  frameSize;
@property (nonatomic, assign, readonly) CGFloat frameBottom;
@property (nonatomic, assign, readonly) CGFloat frameRight;
@property (nonatomic, assign) CGFloat frameCenterX;
@property (nonatomic, assign) CGFloat frameCenterY;

- (void)setScaleX:(CGFloat)x Y:(CGFloat)y;

/**
 * 뷰 회전
 * @param round 회전수(바퀴)
 * @param speed 속도(1~10, 클수록 빠름)
 * @return CGFloat 최종 각도(degree)
 */
- (CGFloat)rotationWithRound:(CGFloat)round speed:(NSInteger)speed;
- (void)startRotating;
- (void)stopRotating;

- (void)sizeToFitForFlexibleHeight;
- (void)sizeToFitForFlexibleWidth;

#pragma mark - 뷰 영역 표시
/*
 *  view        테두리 표시할 뷰
 *  color       테두리 컬러
 *  isInfoShow  view의 0,0에 좌표 및 크기정보 표시
 */
- (void)showViewBorder;
- (void)showViewBorderWithInfo:(BOOL)isInfoShow;
- (void)showViewBorderWithColor:(UIColor *)color;
- (void)showViewBorderWithColor:(UIColor *)color info:(BOOL)isInfoShow;
- (void)showViewBorderOfSubviews;
- (void)showViewBorderOfSubviewsWithInfo:(BOOL)isInfoShow;

@end

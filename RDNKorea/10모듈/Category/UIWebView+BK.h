//
//  UIWebView+BK.h
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWebView (BK)

/*
 *  웹뷰처럼 보이지 않게하기 위해 배경을 투명으로 만든 웹뷰
 */
+ (UIWebView *)webViewOfBGClearWithFrame:(CGRect)frame;

@end

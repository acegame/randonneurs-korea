//
//  RDNUser.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 16..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDNUser : NSObject

+(RDNUser *)shared;
- (void)clear;

// computed
@property (nonatomic, assign, readonly) BOOL isSigned;
@property (nonatomic, assign, readonly) BOOL isIdPsswordExist;

// persistence
@property (nonatomic, copy) NSString *clienId;
@property (nonatomic, copy) NSString *clienPassword;
@property (nonatomic, assign) BOOL shareMyLocation;

/*
 *  이 앱에서 사용자 고유값(ID)으로 사용할 값. 클리앙 로그인시 쿠키값으로 받음.
 *  로그인 성공시 자동저장. persistence아님
 *  누구나 접근할 수 있는 정보로 민감한 정보는 사용하지 말 것
 */
@property (nonatomic, copy) NSString *cookieId;

@end

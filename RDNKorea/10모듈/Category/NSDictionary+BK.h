//
//  NSDictionary+BK.h
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 WAG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (BK)

@end

@interface NSMutableDictionary (BK)
- (void)setSafeValue:(id)value forKey:(id)key;
@end
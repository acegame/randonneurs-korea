//
//  RDNIndicator.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDNIndicator : UIView
+ (void)increase;
+ (void)decrease;
@end

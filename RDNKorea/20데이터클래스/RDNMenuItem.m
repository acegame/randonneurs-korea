//
//  RDNMenuItem.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 3. 10..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "RDNMenuItem.h"

@implementation RDNMenuItem
+ (RDNMenuItem *)menuItemWithTitle:(NSString *)title storyboardIdentifierOfClass:(NSString *)storyboardIdentifier {
    RDNMenuItem *menuItem = [[RDNMenuItem alloc] init];
    menuItem.storyboardIdentifier = storyboardIdentifier;
    menuItem.title = title;
    return menuItem;
}
@end

//
//  Toast.h
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDNToast : NSObject
+ (void)showWithMessage:(NSString *)message;
+ (void)showWithMessage:(NSString *)message image:(NSURL *)url;
@end

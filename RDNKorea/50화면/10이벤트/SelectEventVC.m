//
//  SelectEvent.m
//  RDNKorea
//
//  Created by Ryan Han on 2015. 2. 26..
//  Copyright (c) 2015년 trick14. All rights reserved.
//

#import "SelectEventVC.h"
#import "EventCell.h"
#import "RDNEvent.h"
#import "EventDetailVC.h"

@interface SelectEventVC () <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *tableView_;
    NSMutableArray *dataSource_;
}
@end

@implementation SelectEventVC

#pragma mark - UIViewController Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"랜도너스";
    
    dataSource_ = [NSMutableArray array];
    [self requestRoutes];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource_.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventCell"];
    RDNEvent *event = [dataSource_ objectAtIndex:indexPath.row];
    [cell setEvent:event];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"EventDetailVC"]) {
        RDNEvent *event = [dataSource_ objectAtIndex:[tableView_ indexPathForSelectedRow].row];
        EventDetailVC *dv = (EventDetailVC *)segue.destinationViewController;
        [dv setEvent:event];
    }
}

#pragma mark - Parse, TBXML
- (void)requestRoutes {
    [RDNIndicator increase];
    PFQuery *query = [PFQuery queryWithClassName:@"ROUTES"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error) {
            for(PFObject *object in objects) {
                RDNEvent *event = [[RDNEvent alloc] init];
                event.name = object[@"EVENT_NAME"];
                event.date = object[@"EVENT_DATE"];
                PFFile *routes = object[@"FILE"];
                event.route = routes.url;
                PFFile *image  = object[@"THUMBNAIL"];
                event.thumbnail = image.url;
                [dataSource_ addObject:event];
            }
        }
        [tableView_ reloadData];
        [RDNIndicator decrease];
    }];
}


@end

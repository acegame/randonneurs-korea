//
//  NSArray+BK.m
//  Test
//
//  Created by 한병기 on 2014. 8. 22..
//  Copyright (c) 2014년 아사달. All rights reserved.
//

#import "NSArray+BK.h"

@implementation NSArray (BK)

- (id)objAtIndex:(NSUInteger)index
{
    if(self.count < index + 1)
    {
        return nil;
    }
    else
    {
        return [self objectAtIndex:index];
    }
}

@end
